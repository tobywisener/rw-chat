const express = require('express')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http, {
  cors: {
    origin: '*'
  }
});

const {
  addUser,
  getUsers,
  getUserFromSocketId,
  removeUser
} = require('./users')

const port = process.env.PORT || 3000;

app.use(express.static(__dirname));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', socket => {
  const broadcastUsers = () => {
    io.emit('users', getUsers())
  }

  socket.on('login', data => {
    console.log("Adding user:",data);
    const user = addUser(socket.id, data.name, data.avatarUrl)

    // notify the user who they are
    io.to(socket.id).emit('user', { user })

    // send list of all users
    broadcastUsers()

    // add new user to the users list with a default nickname
    console.log('Added a new user to => ' + JSON.stringify(getUsers()))
  })

  socket.on('disconnect', () => {
    // make new list without the disconnected user
    removeUser(socket.id)
    // send updated list to all users
    broadcastUsers()

    console.log('Current online users => ' + JSON.stringify(getUsers()))
  })

  socket.on('private message', (data, ack) => {
    // get current timestamp
    const currentTimeStamp = Date.now()

    // find the user that belongs to this socket
    const user = getUserFromSocketId(socket.id)

    socket.to(data.id).emit(
      'private message',
      {
        name: user.name,
        time: currentTimeStamp,
        message: data.message,
        from: socket.id
      }
    )
  })

  socket.on('message', (message, ack) => {
    // get current timestamp
    const currentTimeStamp = Date.now()

    // find the user that belongs to this socket
    const user = getUserFromSocketId(socket.id)

    // handle the current message time to stop rate limiting
    if (user.lastTimeStamp) {
      const ms = currentTimeStamp - user.lastTimeStamp
      const secondsElapsed = Math.floor(ms / 1000)

      // has less than 2 seconds passed since last message? rate limit them
      if (secondsElapsed < 2) {
        // update timestamp on user object
        user.lastTimeStamp = currentTimeStamp
        // send an ack
        ack('rl')
        return
      }
    }

    // update timestamp on user object
    user.lastTimeStamp = currentTimeStamp

    io.emit('message', {
      name: user.name,
      time: currentTimeStamp,
      message,
    })
    
    // notify the sender their message has been handled
    ack('ok')
  })

  socket.on('typing', () => {
    // find the user that belongs to this socket
    const user = getUserFromSocketId(socket.id)

    // notify other users thta they are typing
    io.emit('typing', {
      name: user.name
    })
  })
});

http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});
