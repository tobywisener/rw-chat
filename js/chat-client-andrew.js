var socket            = io('//localhost:3000'),
    chatContainer     = document.getElementById('raffle-wise-chat'),
    emojiButton       = document.getElementById('emoji-button'),
    usersList         = document.getElementById('users-list'),
    userCount         = document.getElementById('rw-chat-user-count'),
    input             = document.getElementById('chat-input'),
    typingBox         = document.getElementById('typing-box'),
    messagesContainer = document.getElementById('rw-chat-messages-container'),
    user;

// On doc ready
document.addEventListener('DOMContentLoaded', (event) => {
    if (window.location !== window.parent.location) {
        /* TEMPORARY CODE - REMOVE WHEN RW COMPETITIONS DONT GET REDIRECTED TO IFRAME */
        // Hide the chat button by default inside iframe (we'll show it in parent frame)
        jQuery('#chat-btn').hide();
    }
});

jQuery(document).on("click", ".close-chat", function () {
    chatContainer.classList.remove('show');
    chatContainer.classList.add('hide');
});

jQuery(document).on("click", "#chat-btn", function () {
    chatContainer.classList.remove('hide');
    chatContainer.classList.add('show');
});

jQuery('textarea#chat-input').on('keypress',function(e) {
    var message = input.value;

    if(e.which !== 13 || !message) {
        // Guard clause to end exit early
        console.log("Message send blocked");
        return;
    }

    socket.emit('message', message, (response) => {
        console.log('server sent us ack value: ' + response)

        if (response === 'rl') {
            // we are being rate limited - dont do anything
            return;
        }

        // response === 'ok'
        input.value = '';
        input.disabled = false;
        input.focus();
    });

    // Stop default behaviour AND prevent bubbling up
    return false /* still needs handled */;
});


input.onkeyup = () => {
    // send typing event to server (it will handle the rest)
    socket.emit('typing');
}

socket.on('user', data => {
    // server has sent us info about who we are
    user = data.user;

    console.log(JSON.stringify(user));
})

socket.on('typing', (msg) => {
    // dont do anything, its us typing
    if (msg.name === user.name) {
        return;
    }

    typingBox.textContent = msg.name + ' is typing...';

    // there is a better way to do this
    setTimeout(() => {
        // get rid of it
        typingBox.textContent = '';
    }, 1000);
})

socket.on('connect', msg => {
    // login
    // exclude name param for guest account (name will be generated and returned)
    socket.emit('login', {
        name: RaffleWiseChat.user_name,
        avatarUrl: RaffleWiseChat.user_avatar_url
    });
})

socket.on('users', users => {
    while (usersList.firstChild) {
        usersList.removeChild(usersList.firstChild);
    }

    users.forEach((item) => {
        let el = document.createElement('li');

        el.setAttribute('data-name', item.name);
        el.setAttribute('title', item.name);
        if (item.avatarUrl !== '') {
            el.style.backgroundImage = "url('" + item.avatarUrl + "')";
        }

        // is it us? add class (me)
        if (item.name === user.name) {
            el.classList.add('me');
        }

        usersList.appendChild(el);
    });

    // Update the number of users online
    userCount.innerText = users.length;
})

socket.on('message', function(data) {
    // Add message to the message list (will tidy up)
    messagesContainer.insertAdjacentHTML('beforeend',
        '<div class="WhatsappChat__MessageContainer-sc-1wqac52-1 dAbFpq">\n' +
        '                <div style="opacity: 0;" class="WhatsappDots__Component-pks5bf-0 eJJEeC">\n' +
        '                  <div class="WhatsappDots__ComponentInner-pks5bf-1 hFENyl">\n' +
        '                    <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotOne-pks5bf-3 ixsrax"></div>\n' +
        '                    <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotTwo-pks5bf-4 dRvxoz"></div>\n' +
        '                    <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotThree-pks5bf-5 kXBtNt"></div>\n' +
        '                  </div>\n' +
        '                </div>\n' +
        '                <div style="opacity: 1;" class="WhatsappChat__Message-sc-1wqac52-4 kAZgZq">\n' +
        '                  <div class="WhatsappChat__Author-sc-1wqac52-3 bMIBDo">' + data.name + '</div>\n' +
        '                  <div class="WhatsappChat__Text-sc-1wqac52-2 iSpIQi">' + data.message + '</div>\n' +
        '                  <div class="WhatsappChat__Time-sc-1wqac52-5 cqCDVm">' + data.time + '</div>\n' +
        '                </div>\n' +
        '              </div>');

    messagesContainer.scrollTo(0, messagesContainer.scrollHeight);
});

const picker = new EmojiButton({
    rootElement: chatContainer,
    zIndex: 1002 /* Above chat and sticky nav */
});

picker.on('emoji', emoji => {
    input.value += emoji;

    input.focus();
});

emojiButton.addEventListener('click', () => {
    picker.togglePicker(emojiButton);
});