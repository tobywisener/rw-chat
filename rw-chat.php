<?php
/*
Plugin Name: RaffleWise Chat
Plugin URI: https://rafflewise.ie
Description: Provides a public chatroom for communication
Version: 1.0
Author: Wisener Solutions Limited
Author URI: https://rafflewise.ie

Plugin developed by Wisener Solutions Limited 2021

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

register_activation_hook(__FILE__, 'rw_chat_activate_plugin');
function rw_chat_activate_plugin() {
	global $wpdb;

    /* Messages table
    $sites_table_name = $wpdb->prefix . 'raffle_wise_marketplace_sites';
    if($wpdb->get_var("show tables like '$sites_table_name'") != $sites_table_name)
    {
        $sql = "CREATE TABLE $sites_table_name
                    ( `id` INT NOT NULL AUTO_INCREMENT ,
                    `site_id` VARCHAR(255) NOT NULL ,
                    `site_name` VARCHAR(100) NOT NULL ,
                    `site_icon` VARCHAR(2083) NULL ,
                    `site_logo` VARCHAR(2083) NULL ,
                    PRIMARY KEY (`id`),
                    UNIQUE (`site_id`));";

        $wpdb->query($sql);
    }
    */
}

class RaffleWiseChat {

	// The complete slug for this plugin
	private $plugin_slug = "raffle-wise-chat";

    private $db_sites_table_name = /* WPDB Prefix */ "raffle_wise_chat_messages";

	function __construct() {
		global $wpdb;

        $this->db_sites_table_name = $wpdb->prefix . $this->db_sites_table_name;
	}

	// Attach all hooks and filters
	function run() {

		// Enqueue the CSS & JS
		add_action('wp_enqueue_scripts',array($this, 'enqueue_scripts'));

        // Shortcode for displaying the chat widget
        add_shortcode( 'rw-chat', [$this,'shortcode_chat'] );

        // Append the chat directly to the WP header
        add_action('wp_head', function() {
            echo self::chatHTML();
        });

		add_action( 'rest_api_init', function() {

            /**
            *   Example REST call for primary entity (messages)
            *
            *	register_rest_route(
            *    	'raffle-wise-chat/v1', '',
            *    	[ 'methods' => 'GET', 'callback' => [$this, 'api_get_messages'], 'permission_callback' => '__return_true' ]
            *	);
            */

		});

    }

    /**
     * Enqueue the public scripts
     */
    function enqueue_scripts() {
        $user_id = get_current_user_id();
        $user_name = 'Guest';
        $user_avatar_url = '';
        if($user_id > 0) {
            // If user is logged in, override the display name
            $user_info = get_userdata($user_id);
            $user_name = ucwords($user_info->display_name);
            $user_avatar_url = get_avatar_url( $user_id );
        }

        wp_enqueue_script( 'rw-chat-socket-io',
            plugin_dir_url( __FILE__ ) . '/js/socket.io.min.js',
            array( 'jquery' ),
            '1.0.0', true );

        wp_enqueue_script( 'rw-chat-emoji-buttons',
            plugin_dir_url( __FILE__ ) . '/js/emoji-button.min.js',
            array( 'jquery' ),
            '1.0.0', true );

        wp_enqueue_script( 'rw-chat-scripts',
            plugin_dir_url( __FILE__ ) . '/js/chat-client-andrew.js',
            array( 'jquery' ),
            '1.0.0', true );

        // Create a global JS variable called RaffleWiseChat
        wp_localize_script( 'rw-chat-scripts', 'RaffleWiseChat',
            array(
                'user_id' => $user_id,
                'user_name' => $user_name,
                'user_avatar_url' => $user_avatar_url
            )
        );

        wp_enqueue_style( 'rw-chat-styles',
            plugin_dir_url( __FILE__ ) . '/css/rw-chat.css' );
    }

    /**
     * Produces a calendar of upcoming competitions
     *
     * [rw-chat]
     */
    function shortcode_chat( $atts ) {
        return self::chatHTML();
    }

    /**
     * Returns the HTML for showing the chat button.
     */
    public static function chatHTML() {
        return '
        <div id="raffle-wise-chat" class="hide">
          <div class="header-chat">
            <ul id="users-list">
            </ul>
            <div class="get-new hide">
              <div id="get-label"></div>
              <div id="get-nama"></div>
            </div>
          </div>
          <div class="home-chat">

          </div>
          <div class="start-chat">
            <div id="rw-chat-messages-container" class="WhatsappChat__Component-sc-1wqac52-0 raffle-wise-chat-body">

            </div>

            <div class="blanter-msg">
              <textarea id="chat-input" placeholder="Write a message" maxlength="120" row="1"></textarea>
              <a href="javascript:undefined;" id="send-it"><svg viewBox="0 0 448 448"><path d="M.213 32L0 181.333 320 224 0 266.667.213 416 448 224z"/></svg></a>
              <div id="emoji-button"><i class="fa fa-smile"></i></div>
            </div>
          </div>
          <div id="get-number"></div><a class="close-chat" href="javascript:undefined">×</a>
        </div>

        <!-- Start of chat button -->
        <a id="chat-btn" style="display: inline;" title="Chat coming soon!">
            <span id="rw-chat-user-count" class="gp-notification-counter">0</span>
            <span id="toTopHover" style="opacity: 0;"></span><i class="fa fa-comment"></i>
        </a>';
    }

    function debug_log( $string, $object = NULL ) {
        $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("Y-m-d H:i:s",$date_array[1]);

        echo '['.$date.'] ' . $string . ($object == NULL ? '' : ' ' . print_r($object,true)) . '<br/>';
    }
}

function run_raffle_wise_chat() {

    // Create a single instance of the plugin class for communication throughout Wordpress
	$GLOBALS['RaffleWiseChat'] = new RaffleWiseChat();
	$GLOBALS['RaffleWiseChat']->run();

}
run_raffle_wise_chat();
