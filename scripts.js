var socket = io()

var usersList = document.getElementById('users')
var messages = document.getElementById('messages')
var form = document.getElementById('form')
var input = document.getElementById('input')
var typingBox = document.getElementById('typing-box')

let user

form.addEventListener('submit', (e) => {
  e.preventDefault()

  if (input.value) {
    input.disabled = true

    // send message
    socket.emit('message', input.value, response => {
      if (response === 'rl') {
        // we are being rate limited
        alert('you are being rated limited, stop trying to send so many messages')
      }

      // response === 'ok'
      input.value = ''
      input.disabled = false
      input.focus()
    })
  }
})

input.onkeyup = () => {
  // send typing event to server (it will handle the rest)
  if (input.value.length > 0) {
    socket.emit('typing')
  }
}

socket.on('user', data => {
  // server has sent us info about who we are
  user = data.user

  console.log(JSON.stringify(user))
})

socket.on('typing', data => {
    // dont do anything, its us typing
  if (data.name === user.name) {
    return
  }

  typingBox.textContent = msg.name + ' is typing...'

  // there has to be a better way to do this
  setTimeout(() => {
    // get rid of it
    typingBox.textContent = ''
  }, 1000)
})

socket.on('connect', msg => {
  // login
  // exclude name param for guest account (name will be generated and returned)
  socket.emit('login', {
    name: ''
  })
})

socket.on('users', users => {
  while (usersList.firstChild) {
    usersList.removeChild(usersList.firstChild)
  }

  users.forEach((item) => {
    let el = document.createElement('li')
    
    // set data-user to json of the user's data
    el.setAttribute('data-user', JSON.stringify(item))
    el.textContent = item.name

    // is it us? append (me)
    if (item.name === user.name) {
      el.textContent += ' (me)'
    } else {
      el.onclick = (e) => {
      e.preventDefault()

      let user = JSON.parse(e.target.getAttribute('data-user'))
      let message = prompt('What do you want to send to ' + user.name + '?', '')

      socket.emit('private message', { id: user.id, message }, response => {
        // // not yet implemented for PM
        // if (response === 'rl') {
        //   alert('you are being rated limited, stop trying to send so many messages')
        // }
      })
    }
    }

    usersList.appendChild(el)
  })
})

const handleMessage = data => {
  let listItem = document.createElement('li')

  if (data.from) {
    listItem.textContent += '*PRIVATE* '
  }

  listItem.textContent += `[${data.time}] <${data.name}> ${data.message}`
  messages.appendChild(listItem)
  
  // focus?
  window.scrollTo(0, document.body.scrollHeight)
}

socket.on('private message', data => {
  handleMessage(data)
})

socket.on('message', data => {
  handleMessage(data)
})

// emoji button related functionality

const button = document.querySelector('#emoji-button')
const picker = new EmojiButton()

picker.on('emoji', emoji => {
  input.value += emoji;
})

button.addEventListener('click', () => {
  picker.togglePicker(button)
})