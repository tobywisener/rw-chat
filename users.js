const uniqid = require('uniqid')

let users = []

const addUser = (id, name, avatarUrl) => {
  const user = {
    id: id,
    name: name ? name : 'Guest ' + uniqid.time(),
    guest: !name,
    avatarUrl: avatarUrl
  }
  // add user to array
  users.push(user)

  return user
}

// provide some abstraction
const getUsers = () => {
  return users
}

const getUserFromSocketId = (id) => {
  return users.find((el) => {
    return el.id === id
  })
}

const removeUser = id => {
  let filteredUsers = users.filter(el => el.id !== id)

  if (!filteredUsers) {
    return false
  } else {
    users = filteredUsers
    return true
  }
}

module.exports = {
  addUser,
  getUsers,
  getUserFromSocketId,
  removeUser
}